﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GroupToolMenu : MonoBehaviour
{
    [MenuItem("Editor Tools/Group Tool", false, 1)]
    public static void LaunchGroupTool()
    {
        GroupToolEditor.LaunchGroupWindow();
    }
}
