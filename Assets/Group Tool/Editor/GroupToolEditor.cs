﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GroupToolEditor : EditorWindow
{
    string wantedName = "Enter Name";
    int currentSelectedItems;
    GameObject[] selectedObjects = new GameObject[0];

    public static void LaunchGroupWindow()
    {
        var window = GetWindow<GroupToolEditor>("Group Selected");
        window.Show();
    }

    private void OnGUI()
    {
        selectedObjects = Selection.gameObjects;
        currentSelectedItems = selectedObjects.Length;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Selection Count: " + currentSelectedItems.ToString());

        GUILayout.Space(5);

        EditorGUILayout.LabelField("Group Name", EditorStyles.boldLabel);
        wantedName = EditorGUILayout.TextField(wantedName);

        GUILayout.Space(2);

        if (GUILayout.Button("Group Selected", GUILayout.ExpandWidth(true), GUILayout.Height(45)))
        {
            GroupSelection();
        }

        EditorGUILayout.Space();
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();
        EditorGUILayout.EndHorizontal();

        Repaint();
    }

    void GroupSelection()
    {
        if (selectedObjects.Length > 0)
        {
            if (wantedName != "Enter Name")
            {
                GameObject group = new GameObject(wantedName);

                foreach (var item in selectedObjects)
                {
                    item.transform.SetParent(group.transform);
                }
            }
            else
            {
                EditorUtility.DisplayDialog("GROUPING ERROR!", "PLEASE PROVIDE A NAME FOR THE GROUP!", "OK");
            }
        }
    }
}
