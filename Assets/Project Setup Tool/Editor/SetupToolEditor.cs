﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.PackageManager.UI;

public class SetupToolEditor : EditorWindow
{
    static SetupToolEditor window;

    int noOfTeamMembers = 1;

    string teamMembersCount = "";

    List<string> teammateSubFols = new List<string>{ "Animations", "Images", "Materials", "Placeholders", "Prefabs", "Scenes", "Scripts" };

    public static void LaunchSetupWindow()
    {
        window = GetWindow<SetupToolEditor>("Set Up Project");
        window.Show();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);

        EditorGUILayout.BeginVertical();
        GUILayout.Space(10);

        teamMembersCount = EditorGUILayout.TextField("Number Of Teammates: ", noOfTeamMembers.ToString(), EditorStyles.miniTextField, GUILayout.ExpandWidth(true));

        noOfTeamMembers = int.Parse(teamMembersCount);

        if (GUILayout.Button("Setup Project", GUILayout.Height(45), GUILayout.ExpandWidth(true)))
        {
            CreateFolders();
        }

        GUILayout.Space(10);
        EditorGUILayout.EndVertical();

        GUILayout.Space(10);
        EditorGUILayout.EndHorizontal();

        Repaint();
    }

    void CreateFolders()
    {
        string assetPath = Application.dataPath;
        string rootPath = assetPath;

        DirectoryInfo rootInfo = Directory.CreateDirectory(rootPath);

        if(!rootInfo.Exists)
        {
            return;
        }

        CreateSubFolders(rootPath);

        AssetDatabase.Refresh();

        CloseWindow();
    }

    void CreateSubFolders(string rootPath)
    {
        DirectoryInfo rootInfo = null;
        List<string> folderNames = new List<string>();

        rootInfo = Directory.CreateDirectory(rootPath);
        if (rootInfo.Exists)
        {
            folderNames.Clear();
            for (int i = 1; i <= noOfTeamMembers; i++)
            {
                folderNames.Add("Member_" + i);
                CreateFolder(rootPath + "/" + "Member_" + i, teammateSubFols);
            }
            folderNames.Add("Audio");
            folderNames.Add("Characters");
            folderNames.Add("Editor");
            folderNames.Add("Environment");
            folderNames.Add("Materials");
            folderNames.Add("Prefabs");
            folderNames.Add("Scenes");
            folderNames.Add("Scripts");
            folderNames.Add("VFX");
            CreateFolder(rootPath, folderNames);
        }
    }

    void CreateFolder(string path, List<string> folders)
    {
        foreach (var folder in folders)
        {
            Directory.CreateDirectory(path + "/" + folder);
        }
    }

    void CloseWindow()
    {
        if (window)
        {
            window.Close();
        }
    }
}