﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SetupToolMenu : MonoBehaviour
{
    [MenuItem("Editor Tools/Setup Tool", false, 1)]
    public static void LaunchGroupTool()
    {
        SetupToolEditor.LaunchSetupWindow();
    }
}
