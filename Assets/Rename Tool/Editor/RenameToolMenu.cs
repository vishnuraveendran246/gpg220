﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class RenameToolMenu : MonoBehaviour
{
    [MenuItem("Editor Tools/Rename Tool", false, 1)]
    public static void LaunchGroupTool()
    {
        RenameToolEditor.LaunchRenameWindow();
    }
}
