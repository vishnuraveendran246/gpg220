﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class RenameToolEditor : EditorWindow
{
    GameObject[] selected = new GameObject[0];

    string newName = "";
    string prefix = "";
    string suffix = "";

    bool addNumbers;
    bool useUnderscore;

    public static void LaunchRenameWindow()
    {
        var window = GetWindow<RenameToolEditor>("Rename Selected");
        window.Show();
    }

    private void OnGUI()
    {
        selected = Selection.gameObjects;

        EditorGUILayout.LabelField("Selected: " + selected.Length.ToString("000"));

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(10);

        EditorGUILayout.BeginVertical();
        GUILayout.Space(10);

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.Space(10);

        newName = EditorGUILayout.TextField("New Name: " , newName, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
        prefix = EditorGUILayout.TextField("Prefix: " , prefix, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
        suffix = EditorGUILayout.TextField("Suffix: " , suffix, EditorStyles.miniTextField, GUILayout.ExpandWidth(true));
        addNumbers = EditorGUILayout.Toggle("Add numbers to the end?", addNumbers);
        useUnderscore = EditorGUILayout.Toggle("Use _ instead of space?", useUnderscore);

        GUILayout.Space(10);
        EditorGUILayout.EndVertical();
        
        if(GUILayout.Button("Rename Selected", GUILayout.Height(45) ,GUILayout.ExpandWidth(true)))
        {
            RenameSelected();
        }

        GUILayout.Space(10);
        EditorGUILayout.EndVertical();

        GUILayout.Space(10);
        EditorGUILayout.EndHorizontal();

        Repaint();
    }

    void RenameSelected()
    {
        Array.Sort(selected, delegate (GameObject A, GameObject B) { return A.name.CompareTo(B.name); });

        for (int i = 0; i < selected.Length; i++)
        {
            string finalName = string.Empty;

            if (prefix.Length>0)
            {
                finalName += prefix;
            }

            if (newName.Length > 0)
            {
                if (!useUnderscore)
                {
                    finalName += " " + newName;
                }
                else
                {
                    finalName += "_" + newName;
                }
            }

            if (suffix.Length > 0)
            {
                if (!useUnderscore)
                {
                    finalName += " " + suffix;
                }
                else
                {
                    finalName += "_" + suffix;
                }
            }

            if (addNumbers)
            {
                if (!useUnderscore)
                {
                    finalName += " " + (i+1).ToString("000");
                }
                else
                {
                    finalName += "_" + (i+1).ToString("000");
                }
            }

            selected[i].name = finalName;
        }
    }
}
