﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour
{
    [HideInInspector] public Vector3 destination;
    public float speed;
    public float slowingRadius;

    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Arrive();
    }

    public void Arrive()
    {
        Vector3 distToDest = destination - transform.position;
        Vector3 desiredVel = distToDest.normalized * speed;
        desiredVel.z = 0;

        float slowDownFactor = Mathf.Clamp01(distToDest.magnitude / slowingRadius);

        Vector3 steering = desiredVel - rb.velocity;

        rb.velocity += steering * Time.deltaTime;

        rb.velocity *= slowDownFactor;

        transform.position += rb.velocity * Time.deltaTime;
    }
}