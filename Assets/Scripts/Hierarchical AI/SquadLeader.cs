﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquadLeader : MonoBehaviour
{
    public int numberOfUnits;
    public float circleRadius;

    public GameObject soldier;

    public float speed;
    public float slowingRadius;

    Rigidbody rb;
    List<Soldier> soldiers;

    [HideInInspector] public Vector3 squadTargetPos;

    [HideInInspector] public Formations myFormation;

    private void Start()
    {
        soldiers = new List<Soldier>();
        rb = GetComponent<Rigidbody>();

        #region Checking formation for number of units if not Square/T OR Setting number of units if Square/T
        if (numberOfUnits % 2 == 0)
            numberOfUnits++;
        #endregion

        #region Instantiating units
        for (int i = 0; i < numberOfUnits; i++)
        {
            Soldier currentSoldier = Instantiate(soldier, transform.position, transform.rotation, this.transform).GetComponent<Soldier>();
            soldiers.Add(currentSoldier);
        }
        #endregion
    }

    private void Update()
    {
        Arrive();
        FormationUpdate();
    }

    void FormationUpdate()
    {
        Vector3 destination = Vector3.zero;

        #region Switch case for updating formations
        switch (myFormation)
        {
            case Formations.Circle:
                {
                    float circlePoints = 0;
                    circlePoints = 2 * Mathf.PI / soldiers.Count;
                    destination.z = 0;
                    for (int i = 0; i < soldiers.Count; i++)
                    {
                        destination = (transform.right * Mathf.Sin(i * circlePoints) + transform.up * Mathf.Cos(i * circlePoints));
                        soldiers[i].destination = transform.position + (destination * circleRadius);
                    }
                }
                break;
            case Formations.V:
                {
                    for (int i = 0; i < soldiers.Count; i++)
                    {
                        int modI = i % 2;
                        int side = (modI * 2) - 1;
                        destination = (-transform.up * (i + modI)) + (transform.right * side * (i + modI));
                        soldiers[i].destination = transform.position + destination;
                    }
                }
                break;
            case Formations.Square:
                {
                    int Rows = (int)Mathf.Sqrt(numberOfUnits);
                    int Columns = Rows;

                    for (int y = 0; y < Rows; y++)
                    {
                        for (int x = 0; x < Columns; x++)
                        {
                            int modX = x % 2;
                            int modY = y % 2;
                            int ySide = (modY * 2) - 1;
                            int xSide = (modX * 2) - 1;
                            destination = (-transform.up * xSide * (x + modX)) + (transform.right * ySide * (y + modY));
                            soldiers[Rows * y + x].destination = transform.position + destination;
                        }
                    }
                }
                break;
        }
        #endregion
    }

    public void Arrive()
    {
        Vector3 distToDest = squadTargetPos - transform.position;
        Vector3 desiredVel = distToDest.normalized * speed;
        desiredVel.z = 0;

        float slowDownFactor = Mathf.Clamp01(distToDest.magnitude / slowingRadius);

        Vector3 steering = desiredVel - rb.velocity;

        rb.velocity += steering * Time.deltaTime;

        rb.velocity *= slowDownFactor;

        transform.position += rb.velocity * Time.deltaTime;
    }
}