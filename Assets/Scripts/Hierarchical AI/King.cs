﻿using Microsoft.Win32;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build;
using UnityEngine;

public class King : MonoBehaviour
{
    public List<SquadLeader> Leaders;
    public List<Transform> Targets;

    private void Start()
    {
        for (int i = 0; i < Leaders.Count; i++)
        {
            PickFormation(Leaders[i]);
            int rand = Random.Range(0, Targets.Count);
            Leaders[i].squadTargetPos = Targets[rand].position;
            Targets.RemoveAt(rand);
        }
    }

    void PickFormation(SquadLeader leader)
    {
        int picker = Random.Range(0, 3);
        switch (picker)
        {
            case 0: leader.myFormation = Formations.Circle;
                break;
            case 1:
                leader.myFormation = Formations.V;
                break;
            case 2:
                leader.myFormation = Formations.Square;
                break;
        }
    }
}