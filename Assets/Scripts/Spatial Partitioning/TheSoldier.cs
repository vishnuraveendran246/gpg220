﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheSoldier
{
    [Tooltip("MeshRenderer of the object")]
    public MeshRenderer soldierMeshRenderer;
    [Tooltip("Transform of the soldier")]
    public Transform soldierTrans;
    
    protected float walkSpeed;
        
    public TheSoldier previousSoldier;
    public TheSoldier nextSoldier;
    
    public virtual void Move()
    { }

    public virtual void Move(TheSoldier soldier)
    { }
}