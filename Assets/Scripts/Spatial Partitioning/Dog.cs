﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : TheSoldier
{
    public Dog(GameObject soldierObj, float mapWidth)
    {
        this.soldierTrans = soldierObj.transform;

        this.walkSpeed = 2f;
    }
        
    public override void Move(TheSoldier closestHuman)
    {        
        soldierTrans.rotation = Quaternion.LookRotation(closestHuman.soldierTrans.position - soldierTrans.position); //Rotate towards the closest human

        soldierTrans.Translate(Vector3.forward * Time.deltaTime * walkSpeed); //Move towards the closest human
    }
}