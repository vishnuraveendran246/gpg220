﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanDogController : MonoBehaviour
{
    [Tooltip("The friendly unit prefab")]
    public GameObject friendlyObj;
    [Tooltip("The human unit prefab")]
    public GameObject humanObj;

    [Tooltip("The material for the human")]
    public Material humanMaterial;
    [Tooltip("The material for the closest human")]
    public Material closestHumanMaterial;

    [Tooltip("The empty human parent object that the spawned enemies will be parented to")]
    public Transform humanParent;
    [Tooltip("The empty friendly parent object that the spawned friendlies will be parented to")]
    public Transform friendlyParent;

    [Tooltip("Show grid or not")]
    public bool showGrid;

    List<TheSoldier> humanSoldiers = new List<TheSoldier>();
    List<TheSoldier> friendlySoldiers = new List<TheSoldier>();

    List<TheSoldier> closestHumans = new List<TheSoldier>();

    //Grid data
    [Range(10, 50)]
    public int mapWidth = 50;
    [Range(2, 10)]
    public int cellSize = 10;

    [Tooltip("The amount of soldiers on human team")][Range(2, 50)]
    public int numberOfHumanSoldiers;
    [Tooltip("The amount of soldiers on friendly team")][Range(2, 15)]
    public int numberOfFriendlySoldiers;

    SpatialGrid grid; //The Spatial Partition grid

    public GameObject plane;

    void Start()
    {
        mapWidth = RoundUp(mapWidth);

        //Create a new grid
        grid = new SpatialGrid(mapWidth, cellSize);

        //Add random enemies and friendly and store them in a list
        for (int i = 0; i < numberOfHumanSoldiers; i++)
        {
            //Give the human a random position
            Vector3 randomPos = new Vector3(Random.Range(0f, mapWidth), 0.5f, Random.Range(0f, mapWidth));

            //Create a new human
            GameObject newHuman = Instantiate(humanObj, randomPos, Quaternion.identity) as GameObject;

            //Add the human to a list
            humanSoldiers.Add(new TheHuman(newHuman, mapWidth, grid));

            //Parent it
            newHuman.transform.parent = humanParent;
        }

        for (int i = 0; i < numberOfFriendlySoldiers; i++)
        {
            //Give the friendly a random position
            Vector3 randomPos = new Vector3(Random.Range(0f, mapWidth), 0.5f, Random.Range(0f, mapWidth));

            //Create a new friendly
            GameObject newFriendly = Instantiate(friendlyObj, randomPos, Quaternion.identity) as GameObject;

            //Add the friendly to a list
            friendlySoldiers.Add(new Dog(newFriendly, mapWidth));

            //Parent it 
            newFriendly.transform.parent = friendlyParent;
        }
    }

    void Update()
    {
        plane.transform.localScale = new Vector3(mapWidth / 10, 1, mapWidth / 10);
        plane.transform.position = new Vector3(mapWidth / 2, 0, mapWidth / 2);

        //Move the enemies
        for (int i = 0; i < humanSoldiers.Count; i++)
        {
            humanSoldiers[i].Move();
        }

        //Reset material of the closest humans
        for (int i = 0; i < closestHumans.Count; i++)
        {
            closestHumans[i].soldierMeshRenderer.material = humanMaterial;
        }

        //Reset the list with closest enemies
        closestHumans.Clear();

        //For each friendly, find the closest human and change its color and chase it
        for (int i = 0; i < friendlySoldiers.Count; i++)
        {
            TheSoldier closestHuman = grid.FindClosestHuman(friendlySoldiers[i]);

            //If we found an human
            if (closestHuman != null)
            {
                //Change material
                closestHuman.soldierMeshRenderer.material = closestHumanMaterial;

                closestHumans.Add(closestHuman);

                //Move the friendly in the direction of the human
                friendlySoldiers[i].Move(closestHuman);
            }
        }
    }

    public static int RoundUp(int value)
    {
        return 10 * ((value + 9) / 10);
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;

        if (showGrid)
        {
            for (int i = 0; i <= mapWidth; i += cellSize)
            {
                for (int j = 0; j <= mapWidth; j += cellSize)
                {
                    if (j - cellSize >= 0 && i - cellSize >= 0)
                    {
                        Gizmos.DrawLine(new Vector3(i, 0, j), new Vector3(i, 0, j - cellSize));
                        Gizmos.DrawLine(new Vector3(i, 0, j), new Vector3(i - cellSize, 0, j));
                    }
                }
            }
        }
    }
}