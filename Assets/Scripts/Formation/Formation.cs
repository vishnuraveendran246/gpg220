﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using UnityEngine;

public enum Formations 
{
    Circle,
    V,
    Square,
    T
}
public class Formation : MonoBehaviour
{
    public int numberOfUnits;
    public float circleRadius;

    public GameObject unit;

    Rigidbody rb;
    List<Unit> units;

    public Formations Select_Formation = Formations.Square;

    void Start()
    {
        units = new List<Unit>();
        rb = GetComponent<Rigidbody>();

        #region Checking formation for number of units if not Square/T OR Setting number of units if Square/T
        if (numberOfUnits % 2 == 0)
            numberOfUnits++;
        #endregion

        #region Instantiating units
        for (int i = 0; i < numberOfUnits; i++)
        {
            Unit currentUnit = Instantiate(unit, transform.position, transform.rotation).GetComponent<Unit>();
            units.Add(currentUnit);
        }
        #endregion
    }

    void Update()
    {
        FormationUpdate();
    }

    void FormationUpdate()
    {
        Vector3 destination = Vector3.zero;

        #region Switch case for updating formations
        switch (Select_Formation)
        {
            case Formations.Circle:
                {
                    float circlePoints = 0;
                    circlePoints = 2 * Mathf.PI / units.Count;
                    destination.z = 0;
                    for (int i = 0; i < units.Count; i++)
                    {
                        destination = (transform.right * Mathf.Sin(i * circlePoints) + transform.up * Mathf.Cos(i * circlePoints));
                        units[i].destination = transform.position + (destination * circleRadius);
                    }
                }
                break;
            case Formations.V:
                {
                    for (int i = 0; i < units.Count; i++)
                    {
                        int modI = i % 2;
                        int side = (modI * 2) - 1;
                        destination = (-transform.up * (i + modI)) + (transform.right * side * (i + modI));
                        units[i].destination = transform.position + destination;
                    }
                }
                break;
            case Formations.Square:
                {
                    float Rows = Mathf.Sqrt(numberOfUnits);
                    float Columns = Rows;

                    for (int y = 0; y < Rows; y++)
                    {
                        for (int x = 0; x < Columns; x++)
                        {
                            int modX = x % 2;
                            int modY = y % 2;
                            int ySide = (modY * 2) - 1;
                            int xSide = (modX * 2) - 1;
                            destination = (-transform.up * xSide * (x + modX)) + (transform.right * ySide * (y + modY));
                            units[(int)Rows * y + x].destination = transform.position + destination;
                        }
                    }
                }
                break;
        }
        #endregion
    }
}