﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class TestPriorityManager : MonoBehaviour
{
    Queue<RequestPath> requestQueue = new Queue<RequestPath>(); //Queue of requests
    RequestPath currRequest; //The current request in process

    public static TestPriorityManager instance;
    TestPathfinder pathfinding;

    bool processing;

    private void Awake()
    {
        instance = this;
        pathfinding = GetComponent<TestPathfinder>();
    }

    public static void PathRequest(Vector3 start, Vector3 end, Action<Vector3[], bool> callback) //Takes start, target, and stores the function to do after success
    {
        //Creates a new request (structure variable) with given start and end positions and passes the function reference
        RequestPath newRequest = new RequestPath(start, end, callback);

        instance.requestQueue.Enqueue(newRequest); //Enqueues the request

        instance.TryNext(); //Tries a new request if possible
    }

    void TryNext()
    {
        if (!processing && requestQueue.Count > 0) //If a request is not in process or no request has been passed, then try to find a path
        {
            currRequest = requestQueue.Dequeue(); //Current request is the first element of the request queue
            processing = true; //Sets processing to true
            pathfinding.StartFindPath(currRequest.start, currRequest.end);
        }
    }

    public void RequestComplete(Vector3[] path, bool success)
    {
        currRequest.callback(path, success);
        processing = false;
        TryNext();
    }

    struct RequestPath
    {
        public Vector3 start;
        public Vector3 end;
        public Action<Vector3[], bool> callback;

        //Constructor that takes start, target, and stores the function to do after success and assigns them
        public RequestPath(Vector3 startPoint, Vector3 endPoint, Action<Vector3[], bool> theCallback)
        {
            start = startPoint;
            end = endPoint;
            callback = theCallback;
        }
    }
}
