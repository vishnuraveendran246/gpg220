﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestNode : MonoBehaviour, IHeapItem<TestNode>
{
    #region A* Variables
    public bool walkable;

    [HideInInspector] public int gCost;
    [HideInInspector] public int hCost;

    public TestNode parent;

    int heapIndex;
    #endregion

    #region Common Variables
    [HideInInspector] public Vector3 worldPos;
    [HideInInspector] public float xPos;
    [HideInInspector] public float yPos;
    #endregion

    public void Awake()
    {        
        worldPos = transform.position;
        xPos = transform.position.x;
        yPos = transform.position.z;
    }

    public int fCost { get { return gCost + hCost; } }

    public int HeapIndex
    {
        get { return heapIndex; }
        set { heapIndex = value; }
    }

    public int CompareTo(TestNode node)
    {
        int compare = fCost.CompareTo(node.fCost);

        if (compare == 0)
            compare = hCost.CompareTo(node.hCost);

        return -compare;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<TestUnit>().myNode = this;
        }
    }
}