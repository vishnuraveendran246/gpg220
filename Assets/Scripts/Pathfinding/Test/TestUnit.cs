﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestUnit : MonoBehaviour
{
    [Range(4f, 15f)]
    [Tooltip("Speed of the unit's movement")]
    public float speed = 20f;

    [Range(1f, 5f)]
    [Tooltip("Speed of the unit's rotation")]
    public float rotSpeed = 2f;

    Vector3[] path;

    int targetIndex;

    Transform target;

    [HideInInspector] public TestNode myNode;

    private void Start()
    {        
        target = TestPathfinder.instance.endNode.transform;
        TestPriorityManager.PathRequest(transform.position, target.position, OnPathFound); //Passes the units position and target position and function
    }

    public void OnPathFound(Vector3[] newPath, bool pathFound)
    {
        if (pathFound) //If the bool returns as success, then path is found
        {
            path = newPath; //Path is the vector3[] that is returned
            StopCoroutine(FollowPath()); //Stops the current following coroutine
            StartCoroutine(FollowPath()); //Starts the following coroutine again
        }
    }

    IEnumerator FollowPath()
    {
        targetIndex = 0;

        Vector3 currentPoint = new Vector3();

        if (path.Length > 0)
            currentPoint = path[0];

        while (true)
        {
            //Loop through all waypoints in path
            if (transform.position == currentPoint)
            {
                targetIndex++;

                if (targetIndex >= path.Length)
                {
                    targetIndex = 0;
                    path = new Vector3[0];
                    yield break;
                }

                currentPoint = path[targetIndex];
            }

            //Rotate Towards Next Waypoint
            Vector3 targetDir = currentPoint - this.transform.position;
            float step = rotSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
            transform.rotation = Quaternion.LookRotation(newDir);

            //Move Towards Next Waypoint
            transform.position = Vector3.MoveTowards(transform.position, currentPoint, speed * Time.deltaTime);

            yield return null;
        }
    }

    private void OnDrawGizmos()
    {
        //Draws spheres on the waypoint nodes, and lines between them
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(path[i], 0.5f);

                if (i == targetIndex)
                    Gizmos.DrawLine(transform.position, path[i]);
                else
                    Gizmos.DrawLine(path[i - 1], path[i]);
            }
        }
    }
}
