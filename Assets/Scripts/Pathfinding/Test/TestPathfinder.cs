﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class TestPathfinder : MonoBehaviour
{
    TestPriorityManager priorityManager;

    public bool showGizmos;

    public TestUnit player;
    
    TestNode startNode;
    public TestNode endNode;

    public float neighbourRadius;
    public LayerMask neighbourLayer;

    [HideInInspector] public List<TestNode> grid;
    [HideInInspector] public int side;

    public static TestPathfinder instance;

    private void Awake()
    {
        instance = this;
        side = Mathf.RoundToInt(Mathf.Sqrt(grid.Count));
        priorityManager = GetComponent<TestPriorityManager>();
        grid = FindObjectsOfType<TestNode>().ToList<TestNode>();
        startNode = player.myNode;
    }

    IEnumerator FindPath(Vector3 start, Vector3 end)
    {
        Vector3[] waypoints = new Vector3[0];
        bool foundPath = false;


        if (endNode.walkable)
        {
            Heap<TestNode> openList = new Heap<TestNode>(grid.Count);
            List<TestNode> closedList = new List<TestNode>();

            //Adds starting node to openlist
            openList.Add(startNode);

            while (openList.Count > 0)
            {
                //Current node is assigned by removing first element of openList
                TestNode currentNode = openList.RemoveFirst();

                //Current node is then added to closedList
                closedList.Add(currentNode);

                if (currentNode == endNode)
                {
                    //If current is the endNode then path has been found
                    foundPath = true;
                    break;
                }

                foreach (TestNode neighbour in GetNeighbours(currentNode))
                {
                    //If the node is unwalkable or if it already has been processed then it continues
                    if (!neighbour.walkable || closedList.Contains(neighbour))
                        continue;

                    //Calculates cost to move from current node to neighbour that they can possibly move to
                    int costToGoToNeighbour = currentNode.gCost + CalculateDistance(currentNode, neighbour);

                    //If the calculated cost to go to neighbour is lesser than neighbours gCost value, swap the gCost values, recalculate the hCost and assign the parent node
                    if (costToGoToNeighbour < neighbour.gCost || !openList.Contains(neighbour))
                    {
                        neighbour.gCost = costToGoToNeighbour;
                        neighbour.hCost = CalculateDistance(neighbour, endNode);
                        neighbour.parent = currentNode;

                        //Add the neighbour being checked to the openList
                        if (!openList.Contains(neighbour))
                            openList.Add(neighbour);
                        else
                            openList.UpdateItem(neighbour); //Else if it is already in the list, then update with new values
                    }
                }
            }
        }

        yield return null;

        if (foundPath) // Assign the array of points that the unit needs to follow by calling bactrack function
            waypoints = Backtrack(startNode, endNode);

        priorityManager.RequestComplete(waypoints, foundPath); //Say that request is complete with current waypoints array and foundPath boolean
    }

    Vector3[] Backtrack(TestNode start, TestNode end)
    {
        List<TestNode> path = new List<TestNode>();
        TestNode currentNode = end; //Starts from endNode

        while (currentNode != start)
        {
            if (currentNode == start)
                path.Add(currentNode);

            path.Add(currentNode); //Adds current node to path

            currentNode = currentNode.parent; //Sets current node to the it parent (the node that it came from)
        }

        Vector3[] waypoints = SimplifyPath(path);
        Array.Reverse(waypoints);
        return waypoints;
    }

    Vector3[] SimplifyPath(List<TestNode> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 oldDirection = Vector2.zero;

        for (int i = 1; i < path.Count; i++)
        {
            //Check the new direction the path is going in by checking the curr node and the previous node positions
            Vector2 newDirection = new Vector2(path[i - 1].xPos - path[i].xPos, path[i - 1].yPos - path[i].yPos);

            if (newDirection != oldDirection) //If direction has changed then we add the new waypoint
                waypoints.Add(path[i - 1].worldPos);

            oldDirection = newDirection; //We set the old direction to the current new direction
        }
        return waypoints.ToArray(); //Returns the waypoints after converting to a Vector3[]
    }

    int CalculateDistance(TestNode node1, TestNode node2)
    {
        int xDist = Mathf.RoundToInt(Mathf.Abs(node1.xPos - node2.xPos)); //Calculate the dist on the x axis
        int yDist = Mathf.RoundToInt(Mathf.Abs(node1.yPos - node2.yPos)); //Calculate the dist on the y axis (actually the z)

        //Use formula for distance as a diagonal move costs 1.4 and horizontal costs 1
        if (xDist > yDist)
            return (14 * yDist) + 10 * (xDist - yDist);

        return (14 * xDist) + 10 * (yDist - xDist);
    }

    public void StartFindPath(Vector3 start, Vector3 end)
    {
        StartCoroutine(FindPath(start, end));
    }
    
    public List<TestNode> GetNeighbours(TestNode node)
    {
        List<Collider> neighbourColliders = Physics.OverlapSphere(node.transform.position, neighbourRadius, neighbourLayer).ToList<Collider>();

        List<TestNode> neighbours = new List<TestNode>();

        for (int i = 0; i < neighbourColliders.Count; i++)
        {
            neighbours.Add(neighbourColliders[i].gameObject.GetComponent<TestNode>());
        }

        return neighbours;
    }

    private void OnDrawGizmos()
    {
        if (showGizmos)
        {
            for (int i = 0; i < grid.Count; i++)
            {
                Gizmos.DrawWireSphere(grid[i].transform.position, neighbourRadius);
            }
        }
    }
}
