﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class A_Unit : MonoBehaviour
{
    [Tooltip("Target that the unit will follow")]
    public Transform target;

    [Range(4f, 15f)]
    [Tooltip("Speed of the unit's movement")]
    public float speed = 20f;

    [Range(1f, 5f)]
    [Tooltip("Speed of the unit's rotation")]
    public float rotSpeed = 2f;

    Vector3[] path;

    bool moving;

    Grid grid;

    int targetIndex;
    
    private void Awake()
    {
        grid = FindObjectOfType<Grid>();
    }

    private void Start()
    {
        AStarRequestPriorityManager.PathRequest(transform.position, target.position, OnPathFound); //Passes the units position and target position and function
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            moving = false; //sets moving to false
            StopAllCoroutines(); //stops all running coroutines

            if (transform.position != target.position)
            {
                if (!moving) //only request a new path if currently not moving and not reached target
                    AStarRequestPriorityManager.PathRequest(transform.position, target.position, OnPathFound);
            }
        }
    }

    public void OnPathFound(Vector3[] newPath, bool pathFound)
    {
        if (pathFound) //If the bool returns as success, then path is found
        {
            path = newPath; //Path is the vector3[] that is returned
            StopCoroutine(FollowPath()); //Stops the current following coroutine
            moving = false; //sets mmoving to false
            StartCoroutine(FollowPath()); //Starts the following coroutine again
        }
    }

    IEnumerator FollowPath()
    {
        targetIndex = 0;

        Vector3 currentPoint = grid.NodeFromWorldPoint(target.position).worldPos;

        if (path.Length > 0)
             currentPoint = path[0];

        while (true)
        {
            //Loop through all waypoints in path
            if (transform.position == currentPoint)
            {
                targetIndex++;

                if (targetIndex >= path.Length)
                {
                    targetIndex = 0;
                    path = new Vector3[0];
                    yield break;
                }

                currentPoint = path[targetIndex];
            }

            //Rotate Towards Next Waypoint
            Vector3 targetDir = currentPoint - this.transform.position;
            float step = rotSpeed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);
            transform.rotation = Quaternion.LookRotation(newDir);

            //Move Towards Next Waypoint
            transform.position = Vector3.MoveTowards(transform.position, currentPoint, speed * Time.deltaTime);           

            yield return null;
        }
    }

    private void OnDrawGizmos()
    {
        //Draws spheres on the waypoint nodes, and lines between them
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(path[i], 0.5f);

                if (i == targetIndex)
                    Gizmos.DrawLine(transform.position, path[i]);
                else
                    Gizmos.DrawLine(path[i - 1], path[i]);
            }
        }
    }
}