﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heap<T> where T : IHeapItem<T>
{
    T[] items;
    int currentNoOfItems;

    public Heap(int maxSize)
    {
        items = new T[maxSize];
    }

    public void Add(T item)
    {
        item.HeapIndex = currentNoOfItems;
        items[currentNoOfItems] = item;
        MoveUp(item);
        currentNoOfItems++;
    }

    public void MoveUp(T item)
    {
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parent = items[parentIndex];
            if (item.CompareTo(parent) > 0)
            {
                Swap(item, parent);
            }
            else
                break;

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    public void Swap(T item1, T item2)
    {
        items[item1.HeapIndex] = item2;
        items[item2.HeapIndex] = item1;

        int temp = item1.HeapIndex;
        item1.HeapIndex = item2.HeapIndex;
        item2.HeapIndex = temp;
    }

    public T RemoveFirst()
    {
        T first = items[0];
        currentNoOfItems--;
        items[0] = items[currentNoOfItems];
        items[0].HeapIndex = 0;
        MoveDown(items[0]);
        return first;
    }

    public void MoveDown(T item)
    {
        while (true)
        {
            int leftChildIndex = item.HeapIndex * 2 + 1;
            int rightChildIndex = item.HeapIndex * 2 + 2;
            int swapIndex = 0;

            if (leftChildIndex < currentNoOfItems)
            {
                swapIndex = leftChildIndex;

                if (rightChildIndex < currentNoOfItems)
                {
                    if (items[leftChildIndex].CompareTo(items[rightChildIndex]) < 0)
                    {
                        swapIndex = rightChildIndex;
                    }
                }
                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                    return;
            }
            else
                return;
        }
    }

    public bool Contains(T item)
    {
        return Equals(items[item.HeapIndex], item);
    }

    public int Count
    {
        get { return currentNoOfItems; }
    }

    public void UpdateItem(T item)
    {
        MoveUp(item);
    }
}

public interface IHeapItem<T>: IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}