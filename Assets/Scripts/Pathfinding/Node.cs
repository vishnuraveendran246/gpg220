﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : IHeapItem<Node>
{
    #region A* Variables
    public bool walkable;

    public int gCost;
    public int hCost;

    public Node parent;

    int heapIndex;
    #endregion

    #region Common Variables
    public Vector3 worldPos;

    public int xPos;
    public int yPos;
    #endregion

    public Node(bool isWalkable, Vector3 worldPosition, int x, int y)
    {
        walkable = isWalkable;
        worldPos = worldPosition;
        xPos = x;
        yPos = y;
    }

    public int fCost { get { return gCost + hCost; } }

    public int HeapIndex
    {
        get { return heapIndex; }
        set { heapIndex = value; }
    }

    public int CompareTo(Node node)
    {
        int compare = fCost.CompareTo(node.fCost);

        if (compare == 0)
            compare = hCost.CompareTo(node.hCost);

        return -compare;
    }
}