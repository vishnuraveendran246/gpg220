﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UI;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [Tooltip("Whether to show grid or not")]
    public bool showGrid;

    [Tooltip("The units in the scene")]
    public Transform[] units;

    [Tooltip("Target that the units will follow")]
    public Transform target;

    [Tooltip("Layer's that are unwalkable")]
    public LayerMask unwalkable;

    [Tooltip("Size of the grid")]
    public Vector2 gridSize;

    [Tooltip("Radius of each node")]
    public float radius;

    [HideInInspector] public Node[,] grid;

    float diameter;
    int width, height;

    //Get function that gives the number of nodes
    public int MaxSize { get { return width * height; } } 

    private void Awake()
    {
        diameter = radius * 2;
        width = Mathf.RoundToInt(gridSize.x / diameter);
        height = Mathf.RoundToInt(gridSize.y / diameter);
        DrawGrid();
    }

    void DrawGrid()
    {
        grid = new Node[width, height];
        
        //Calculates bottom left using given width and height
        Vector3 bottomLeft = transform.position - Vector3.right * gridSize.x / 2 - Vector3.forward * gridSize.y / 2; 

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                //Calculates current node position by incrementing along the diameter of the node on both y and x axis
                Vector3 worldPoint = bottomLeft + Vector3.right * (i * diameter + radius) + Vector3.forward * (j * diameter + radius); 
                
                //Checks if the node is walkable by checking for any unwalkables in the current radius 
                bool walkable = !Physics.CheckSphere(worldPoint, radius, unwalkable); 
                
                //Creates node in calculated position at [i,j]th element
                grid[i, j] = new Node(walkable, worldPoint, i, j); 
            }
        }
    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        //Searching in a 3x3 block 
        // [-1,-1] [+0,-1] [+1,-1]
        // [-1,+0] [+0,+0] [+1,+0] 
        // [-1,+1] [+0,+1] [+1,+1]
        //Where [+0,+0] is the node itself
        for (int i = -1; i <= 1; i++) 
        {
            for (int j = -1; j <= 1; j++)
            {
                if (i == 0 && j == 0)
                    continue;

                //The current x value that is being checked is the given nodes x + the current i value
                int currX = node.xPos + i; 

                //The current y value that is being checked is the given nodes y + the current j value
                int currY = node.yPos + j; 

                if (currX >= 0 && currX < width && currY >= 0 && currY < height)
                {
                    //Adds the neighbour
                    neighbours.Add(grid[currX, currY]); 
                }
            }
        }

        return neighbours;
    }

    public Node NodeFromWorldPoint(Vector3 wPos)
    {
        //The current xPercentage to check if its on left/middle/right by adding half the size of grid on x and then dividing by the grid size x
        float xPercent = (wPos.x + gridSize.x / 2) / gridSize.x; 

        //The current yPercentage to check if its on top/middle/bottom by adding half the size of grid on yand then dividing by the grid size y
        float yPercent = (wPos.z + gridSize.y / 2) / gridSize.y; 

        //Clamps between 0-1 so that it stays within the grid and not outside to avoid null error
        xPercent = Mathf.Clamp01(xPercent); 

        //Clamps between 0-1 so that it stays within the grid and not outside to avoid null error
        yPercent = Mathf.Clamp01(yPercent); 

        //Subtract 1 so that its not outside the array and assign the x value
        int x = Mathf.RoundToInt((width - 1) * xPercent);  

        //Subtract 1 so that its not outside the array and assign the y value
        int y = Mathf.RoundToInt((height - 1) * yPercent); 

        //Returns the node
        return grid[x, y]; 
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridSize.x, 1, gridSize.y));
        if (grid != null && showGrid)
        {
            List<Node> unitNodes = new List<Node>();
            for (int i = 0; i < units.Length; i++)
            {
                unitNodes.Add(NodeFromWorldPoint(units[i].position));
            }

            Node targetNode = NodeFromWorldPoint(target.position);
            foreach (var node in grid)
            {
                Gizmos.color = (node.walkable) ? Color.yellow : Color.red;
                for (int i = 0; i < units.Length; i++)
                {
                    if (unitNodes[i] == node || targetNode == node)
                        Gizmos.color = Color.cyan;
                }
                Gizmos.DrawCube(node.worldPos, Vector3.one * (diameter - .1f));
            }
        }
    }
}