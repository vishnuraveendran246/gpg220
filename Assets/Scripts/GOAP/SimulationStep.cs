﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationStep
{
    public StatesCollection worldState;
    public GOAPAction action;
    public int depth;
    public float cost;
    public SimulationStep parentStep;

    public SimulationStep(StatesCollection _state, GOAPAction _action, int _depth, float _cost, SimulationStep _fromStep)
    {
        worldState = _state;
        action = _action;
        depth = _depth;
        cost = _cost;
        parentStep = _fromStep;
    }
}