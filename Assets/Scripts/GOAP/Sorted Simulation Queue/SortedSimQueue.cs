﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortedSimQueue
{
    SimQueueElement first;

    public void Enqueue(SimulationStep step, float cost)
    {
        SimQueueElement newElement = new SimQueueElement(step, cost);

        if (first == null)
            first = newElement;
        else
            if (cost < first.totalCost)
        {
            newElement.next = first;
            first = newElement;
        }
        else
        {
            SimQueueElement temp = first;
            while (temp.next != null && cost > temp.totalCost)
                temp = temp.next;
            newElement.next = temp.next;
            temp.next = newElement;
        }
    }

    public SimulationStep Dequeue()
    {
        SimulationStep temp = first.step;
        first = first.next;
        return temp;
    }

    public bool isEmpty()
    {
        return first == null;
    }
}