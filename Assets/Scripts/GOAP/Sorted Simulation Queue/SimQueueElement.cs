﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimQueueElement
{
    public SimulationStep step;
    public float totalCost;
    public SimQueueElement next;

    public SimQueueElement(SimulationStep _step, float _totalCost)
    {
        step = _step;
        totalCost = _totalCost;
    }
}