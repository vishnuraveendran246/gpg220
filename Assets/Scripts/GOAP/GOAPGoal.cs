﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GOAPGoal : MonoBehaviour
{
    string goalName;
    public StatesCollection desiredState;

    private void Start()
    {
        goalName = this.GetType().Name;
    }

    public abstract bool isValid(GOAPAgent agent);
}