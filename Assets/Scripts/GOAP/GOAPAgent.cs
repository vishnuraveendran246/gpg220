﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAPAgent : MonoBehaviour
{
    public StatesCollection worldState;

    List<GOAPGoal> goals;

    List<GOAPAction> myActions;
    Stack<GOAPAction> currentPlan;

    GOAPAction currentAction;

    public int planDepth;
    public int currentGoal;

    public Transform target;
    public Transform inRangePos;

    [HideInInspector] public Scapegoat scapegoat;

    [Range(0, 5)]
    public float speed;

    [Range(1, 10)]
    public float energy;

    [HideInInspector] public Renderer renderer;

    public Material awakeMaterial;
    public Material asleepMaterial;

    private void Start()
    {
        renderer = GetComponent<Renderer>();
        myActions = new List<GOAPAction>(GetComponents<GOAPAction>());
        goals = new List<GOAPGoal>(GetComponents<GOAPGoal>());
        currentPlan = new Stack<GOAPAction>();
        scapegoat = target.GetComponent<Scapegoat>();
    }

    private void Update()
    {
        if ((currentAction == null || currentAction.RunAction(this)))
        {
            if (currentPlan.Count != 0)
            {
                currentAction = currentPlan.Pop();
            }
            else
                currentAction = null;
        }

        if (currentGoal >= goals.Count)
            currentGoal = goals.Count - 1;

        if (!goals[currentGoal].isValid(this))
        {
            for (int i = 0; i < goals.Count; i++)
            {
                if (goals[i].isValid(this))
                {
                    if (currentGoal != i)
                    {
                        currentGoal = i;
                        Plan();
                    }
                }
                break;
            }
        }

        if (currentPlan.Count == 0)
            Plan();
    }
    public void Plan()
    {
        currentPlan.Clear();

        SortedSimQueue simulations = new SortedSimQueue();

        simulations.Enqueue(new SimulationStep(new StatesCollection(worldState), null, 0, 0, null), 0);

        int minDepth = int.MaxValue;
        float minCost = float.MaxValue;

        int noOfIterations = 0;

        while (!simulations.isEmpty())
        {
            noOfIterations++;
            SimulationStep currentStep = simulations.Dequeue();

            if (currentStep.depth > minDepth)
                continue;

            if (currentStep.worldState.CompareStates(goals[currentGoal].desiredState) == 0 || currentStep.depth >= planDepth - 1)
            {
                if (currentStep.cost < minCost || (currentStep.cost == minCost && currentStep.depth < minDepth))
                {
                    currentPlan.Clear();
                    minDepth = currentStep.depth;
                    minCost = currentStep.cost;
                    SimulationStep backtrack = currentStep;

                    while (backtrack.action != null)
                    {
                        currentPlan.Push(backtrack.action);
                        backtrack = backtrack.parentStep;
                    }
                }
            }
            else
            {
                for (int i = 0; i < myActions.Count; i++)
                {
                    if (worldState.CompareStates(myActions[i].requiredStates) == 0 && myActions[i].isValid(currentStep.worldState))
                    {
                        StatesCollection newSimState = new StatesCollection(currentStep.worldState);
                        float newCost = currentStep.cost + myActions[i].GetCost();
                        float hCost = currentStep.worldState.CompareStates(goals[currentGoal].desiredState);
                        simulations.Enqueue(new SimulationStep(newSimState, myActions[i], currentStep.depth + 1, newCost, currentStep), newCost + hCost);
                    }
                }
            }
        }
    }
    public void SpendEnergy(float nrg)
    {
        energy -= nrg;
    }
    public void MoveInRange(string runOrWalk)
    {
        switch (runOrWalk)
        {
            case "Run":
                transform.position = Vector3.MoveTowards(transform.position, inRangePos.position, Time.deltaTime * speed * 2);
                break;
            case "Walk":
                transform.position = Vector3.MoveTowards(transform.position, inRangePos.position, Time.deltaTime * speed);
                break;
        }
    }
    public bool HasEnergy(float nrg)
    {
        if (energy - nrg != 0)
            return true;

        return false;
    }
}