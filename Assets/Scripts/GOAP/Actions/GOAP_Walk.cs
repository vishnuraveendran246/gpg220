﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_Walk : GOAPAction
{
    public override bool isValid(StatesCollection worldState)
    {
        return worldState.HasState(requiredStates);
    }

    public override bool RunAction(GOAPAgent agent)
    {
        Debug.Log("I AM WALKING!");

        agent.MoveInRange("Walk");
        agent.worldState.AddState("isWalking");

        if (agent.transform.position == agent.inRangePos.position)
        {
            agent.worldState.RemoveState("isWalking");
            agent.worldState.AddState("inRange");
            Debug.Log("I AM IN RANGE!");
            agent.SpendEnergy(priority);
            return true;
        }

        return false;
    }
}