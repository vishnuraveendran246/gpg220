﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_Attack : GOAPAction
{
    public int damage;

    public override bool isValid(StatesCollection worldState)
    {
        return worldState.HasState(requiredStates);
    }

    public override bool RunAction(GOAPAgent agent)
    {
        if (isValid(agent.worldState))
        {
            if (agent.HasEnergy(priority))
            {
                agent.scapegoat.TakeDamage(damage);
                agent.worldState.AddState("isAttacking");
                Debug.Log("I AM ATTACKING!");
            }

            else
                return false;
        }
        else
        {
            agent.worldState.RemoveState("isAttacking");
            Debug.Log("I STOPPED ATTACKING!");
        }

            return true;
    }
}
