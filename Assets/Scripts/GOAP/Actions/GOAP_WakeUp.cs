﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_WakeUp : GOAPAction
{
    public override bool isValid(StatesCollection worldState)
    {
        return worldState.HasState(requiredStates);
    }

    public override bool RunAction(GOAPAgent agent)
    {
        Debug.Log("I AM AWAKE!");

        StartCoroutine(wake(agent));

        if (agent.renderer.sharedMaterial == agent.awakeMaterial)
        {
            agent.worldState.AddState("isAwake");
            agent.worldState.RemoveState("isAsleep");
            return true;
        }

        return false;
    }

    IEnumerator wake(GOAPAgent agent)
    {
        yield return new WaitForSeconds(2);
        agent.renderer.sharedMaterial = agent.awakeMaterial;
    }
}
