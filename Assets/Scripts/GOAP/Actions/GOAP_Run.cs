﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_Run : GOAPAction
{
    public override bool isValid(StatesCollection worldState)
    {
        return worldState.HasState(requiredStates);
    }

    public override bool RunAction(GOAPAgent agent)
    {
        Debug.Log("I AM RUNNING!");

        agent.MoveInRange("Run");
        agent.worldState.AddState("isRunning");

        if (agent.transform.position == agent.inRangePos.position)
        {
            agent.worldState.RemoveState("isRunning");
            agent.worldState.AddState("inRange");
            Debug.Log("I AM IN RANGE!");
            agent.SpendEnergy(priority);
            return true;
        }

        return false;
    }
}