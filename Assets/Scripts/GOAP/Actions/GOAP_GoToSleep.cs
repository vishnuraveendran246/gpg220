﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_GoToSleep : GOAPAction
{
    public override bool isValid(StatesCollection worldState)
    {
        return worldState.HasState(requiredStates);
    }

    public override bool RunAction(GOAPAgent agent)
    {
        Debug.Log("I AM ASLEEP!");

        StartCoroutine(sleep(agent));

        if (agent.renderer.sharedMaterial == agent.asleepMaterial)
        {
            agent.worldState.AddState("isAsleep");
            agent.worldState.RemoveState("isAwake");
            return true;
        }

        return false;
    }

    IEnumerator sleep(GOAPAgent agent)
    {
        yield return new WaitForSeconds(2);
        agent.renderer.sharedMaterial = agent.asleepMaterial;
    }
}
