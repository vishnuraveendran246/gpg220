﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_G_Kill : GOAPGoal
{
    public override bool isValid(GOAPAgent agent)
    {        
        return agent.worldState.HasState("enemyIsAlive");
    }
}
