﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOAP_G_Awake : GOAPGoal
{
    public override bool isValid(GOAPAgent agent)
    {
        if (agent.worldState.HasState("isAsleep"))
            return true;

        return false;
    }
}
