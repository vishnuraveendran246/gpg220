﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scapegoat : MonoBehaviour
{
    public float health = 100f;

    public GOAPAgent agent;

    public void TakeDamage(int dmg)
    {
        health -= dmg;
    }

    private void Update()
    {
        if (health < 0)
        {
            StartCoroutine(KillSelf());
        }
    }

    IEnumerator KillSelf()
    {
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
        agent.worldState.RemoveState("enemyIsAlive");
        agent.worldState.RemoveState("inRange");
        agent.worldState.AddState("enemyIsDead");
    }
}
