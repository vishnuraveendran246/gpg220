﻿using UnityEngine;

public abstract class GOAPAction : MonoBehaviour
{
    public StatesCollection requiredStates;

    public float priority;

    public abstract bool RunAction(GOAPAgent agent);

    public virtual bool isValid(StatesCollection worldState)
    {
        return false;
    }

    public float GetCost()
    {
        return priority;
    }
}