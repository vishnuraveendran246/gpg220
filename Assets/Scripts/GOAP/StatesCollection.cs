﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StatesCollection
{
    [SerializeField]
    List<string> states;

    public StatesCollection()
    {
        states = new List<string>();
    }

    public StatesCollection(StatesCollection toCopy)
    {
        states = toCopy.states;
    }

    public int CompareStates(StatesCollection subset)
    {
        int difference = 0;

        foreach (var state in subset.states)
        {
            if (!states.Contains(state))
                difference++;
        }

        return difference;
    }

    public bool HasState(StatesCollection _states)
    {
        foreach (string state in _states.states)
        {
            if (!states.Contains(state))
                return false;
        }

        return true;
    }

    public bool HasState(string state)
    {
        return states.Contains(state);
    }

    public void AddState(string state)
    {
        //Debug.Log("Added " + state);
        if(!states.Contains(state))
            states.Add(state);
    }

    public void RemoveState(string state)
    {
        //Debug.Log("Removed " + state);
        states.Remove(state);
    }
}