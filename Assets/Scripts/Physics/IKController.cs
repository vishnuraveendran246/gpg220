﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class IKController : MonoBehaviour
{
    int chainLength;

    Transform target;
        
    int iterations;
                  
    float delta;

    float[] segmentsLength;
    float completeLength;
    Transform[] segments;
    Vector3[] positions;
    Vector3[] startDirectionSuccessor;
    Quaternion[] startRotationSegment;
    Quaternion startRotationTarget;
    Transform root;


    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Target").transform;

        chainLength = SegmentSpawner.instance.numberOfSegments - 1;
        iterations = SegmentSpawner.instance.iterations;
        delta = SegmentSpawner.instance.delta;

        Initialize();
    }

    void Initialize()
    {
        segments = new Transform[chainLength + 1]; //The segments present in the current structure
        positions = new Vector3[chainLength + 1]; //The positions of the segments
        segmentsLength = new float[chainLength]; //The lengths of each segment
        startDirectionSuccessor = new Vector3[chainLength + 1];
        startRotationSegment = new Quaternion[chainLength + 1];

        root = transform;

        for (int i = 0; i <= chainLength; i++)
        {
            if (root == null)
                throw new UnityException("There is no root!");

            root = root.parent;
        }

        if (target == null)
        {
            target = new GameObject(gameObject.name + " Target").transform;
            SetSegmentPosition(target, GetSegmentPosition(transform));
        }

        startRotationTarget = GetSegmentRotation(target); //sets the start rotation of the target

        Transform current = transform;

        completeLength = 0;
        for (int i = segments.Length - 1; i >= 0; i--)
        {
            segments[i] = current;
            startRotationSegment[i] = GetSegmentRotation(current); //sets the starting rotation of the segment

            if (i == segments.Length - 1)
            {
                //Checking if it is the final segment
                startDirectionSuccessor[i] = GetSegmentPosition(target) - GetSegmentPosition(current);
            }
            else
            {
                startDirectionSuccessor[i] = GetSegmentPosition(segments[i + 1]) - GetSegmentPosition(current); //Distance vector between current and previous segment
                segmentsLength[i] = startDirectionSuccessor[i].magnitude; //Length is the magnitude of the distance vector
                completeLength += segmentsLength[i]; //Add the segment length to the complete length
            }

            current = current.parent; //Set current segment back to its parent segment
        }
    }

    void Update()
    {
        chainLength = SegmentSpawner.instance.numberOfSegments - 1;

        InverseKinematics();
    }

    private void InverseKinematics()
    {
        if (target == null) //If no target is found, then stop
            return;

        if (segmentsLength.Length != chainLength) //If the amount of segments is not the same as the length of the chain, we have to initialize again
            Initialize();

        for (int i = 0; i < segments.Length; i++)
            positions[i] = GetSegmentPosition(segments[i]);

        Vector3 targetPosition = GetSegmentPosition(target);
        Quaternion targetRotation = GetSegmentRotation(target);


        if ((targetPosition - GetSegmentPosition(segments[0])).sqrMagnitude >= completeLength * completeLength) //Check if it is possible to reach the first
        {
            Vector3 direction = (targetPosition - positions[0]).normalized;

            for (int i = 1; i < positions.Length; i++)
                positions[i] = positions[i - 1] + direction * segmentsLength[i - 1]; //My new position is the position of my previous + direction times the length of segment
        }
        else
        {
            for (int i = 0; i < positions.Length - 1; i++)
                positions[i + 1] = Vector3.Lerp(positions[i + 1], positions[i] + startDirectionSuccessor[i], 0);

            for (int iteration = 0; iteration < iterations; iteration++)
            {
                for (int i = positions.Length - 1; i > 0; i--)
                {
                    //loop iterates backwards from last segment
                    if (i == positions.Length - 1)
                        positions[i] = targetPosition; //We are at the start, so we set the position to the target position
                    else
                        positions[i] = positions[i + 1] + (positions[i] - positions[i + 1]).normalized * segmentsLength[i]; 
                        //The position is the pos of previous + the normalized distance vector times the length of segment
                }

                for (int i = 1; i < positions.Length; i++)
                {
                    positions[i] = positions[i - 1] + (positions[i] - positions[i - 1]).normalized * segmentsLength[i - 1];
                    //same as the previous loop, but iterates forward from first segment
                }

                if ((positions[positions.Length - 1] - targetPosition).sqrMagnitude < delta * delta) //We break if the distance to target is smaller than the specified delta
                    break;
            }
        }

        for (int i = 0; i < positions.Length; i++)
        {
            if (i == positions.Length - 1)
                SetSegmentRotation(segments[i], Quaternion.Inverse(targetRotation) * startRotationTarget * Quaternion.Inverse(startRotationSegment[i]));
            else
                SetSegmentRotation(segments[i], Quaternion.FromToRotation(startDirectionSuccessor[i], positions[i + 1] - positions[i]) * Quaternion.Inverse(startRotationSegment[i]));
                //my new rotation is my old rotation multiplied by how much my successor rotated

            SetSegmentPosition(segments[i], positions[i]);
        }
    }

    public Vector3 GetSegmentPosition(Transform current)
    {
        //Gets position based on the rotation of root segment
        if (root == null)
            return current.position;
        else
            return Quaternion.Inverse(root.rotation) * (current.position - root.position);
    }

    public Quaternion GetSegmentRotation(Transform current)
    {
        //Gets rotation based on the rotation of root segment
        if (root == null)
            return current.rotation;
        else
            return Quaternion.Inverse(current.rotation) * root.rotation; //if root is not null, return the rotation of the root multiplied with inverse of current rotation
    }

    public void SetSegmentPosition(Transform current, Vector3 position)
    {
        //Sets position based on the rotation of root segment
        if (root == null)
            current.position = position;
        else
            current.position = root.rotation * position + root.position;
    }
    
    public void SetSegmentRotation(Transform current, Quaternion rotation)
    {
        //Sets rotation based on the rotation of root segment
        if (root == null)
            current.rotation = rotation;
        else
            current.rotation = root.rotation * rotation;
    }
}