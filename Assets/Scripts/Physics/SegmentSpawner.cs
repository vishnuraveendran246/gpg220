﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SegmentSpawner : MonoBehaviour
{
    public static SegmentSpawner instance;

    public GameObject segmentPrefab;

    [Range(2, 500)]
    public int numberOfSegments;

    [Range(0.1f, 1)]
    public float gapBtwSegments;

    [Header("Solver Parameters")]
    public int iterations = 10; //Iterations per update
    public float delta = 0.001f; //Distance at which the solver stops

    private void Awake()
    {
        instance = this;
        SpawnSegments();
    }

    public void SpawnSegments()
    {
        GameObject previous = null;

        for (int i = 0; i < numberOfSegments; i++)
        {
            GameObject currentSegment;

            if (previous != null)
            {
                currentSegment = Instantiate(segmentPrefab, previous.transform.position + new Vector3(0, gapBtwSegments), Quaternion.identity, previous.transform);
                currentSegment.name = "Segment" + (i + 1);

                if (i == numberOfSegments - 1)
                {
                    currentSegment.AddComponent(typeof(IKController));
                }
            }
            else
            {
                currentSegment = Instantiate(segmentPrefab, this.transform);
                currentSegment.name = "Segment" + (i + 1);
            }

            previous = currentSegment;
        }
    }
}