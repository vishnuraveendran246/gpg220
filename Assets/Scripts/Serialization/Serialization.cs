﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;

[Serializable]
class ToSerialize
{
    public string name;
    public int year;
    public float volume;

    public ToSerialize(string _name, int _year, float _volume)
    {
        name = _name;
        volume = _volume;
        year = _year;
    }
}

public class Serialization : MonoBehaviour
{
    ToSerialize toSerialize;
    ToSerialize toLoad;

    public Slider volumeSlider1;
    public InputField yearField1;
    public InputField nameField1;
    public Slider volumeSlider2;
    public InputField yearField2;
    public InputField nameField2;
    BinaryFormatter binaryFormatter = new BinaryFormatter();

    public void SaveToBinary()
    {
        FileStream fs = new FileStream("songInfo.dat", FileMode.Create);

        if (nameField1.text != "" && yearField1.text != "")
            toSerialize = new ToSerialize(nameField1.text, int.Parse(yearField1.text), volumeSlider1.value);
        else
        {
            fs.Close();
            return;
        }

        try
        {
            binaryFormatter.Serialize(fs, toSerialize); 
            nameField1.text = "";
            yearField1.text = "";
            volumeSlider1.value = 0;
        }
        catch (SerializationException e)
        {
            Debug.LogError("Failed to serialize. Reason: " + e.Message);
            throw;
        }

        fs.Close();
    }

    public void LoadFromBinary()
    {
        if (!File.Exists("songInfo.dat"))
            return;

        FileStream fs = new FileStream("songInfo.dat", FileMode.Open);

        if (fs.Length != 0)
        {
            toLoad = (ToSerialize)binaryFormatter.Deserialize(fs);
            nameField2.text = toLoad.name;
            yearField2.text = toLoad.year.ToString();
            volumeSlider2.value = toLoad.volume;
            binaryFormatter.Serialize(fs, toLoad);
        }

        fs.Close();
    }

    public void Clear()
    {
        nameField2.text = "";
        yearField2.text = "";
        volumeSlider2.value = 0;
        nameField1.text = "";
        yearField1.text = "";
        volumeSlider1.value = 0;

        try
        {
            File.Delete("songInfo.dat");
        }
        catch(Exception e)
        {
            Debug.LogError("Failed to clear. Reason: " + e.Message);
            throw;
        }
    }
}