﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deform : MonoBehaviour
{
    [Range(1f, 100f)]
    public float bounceSpeed;

    [Range(1f, 150f)]
    public float fallForce;

    [Range(1f, 20f)]
    public float stiffness;

    public LayerMask layersToCheck;

    [Range(1f, 100f)]
    public float pressureToApply;

    private MeshFilter meshFilter;
    private MeshCollider meshCollider;
    private Mesh mesh;

    Vertex[] vertices;
    Vector3[] currMeshVertices;

    Camera mainCam;

    private void Start()
    {
        mainCam = Camera.main;

        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        mesh = meshFilter.mesh;

        GetVertices();
    }

    private void Update()
    {
        RaycastHit hit;
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Physics.Raycast(ray, out hit, layersToCheck))
            {
                ApplyPressureToPoint(hit.point, pressureToApply);
            }
        }

        UpdateVertices();
    }

    void GetVertices()
    {
        int mvl = mesh.vertices.Length;
        vertices = new Vertex[mvl];
        currMeshVertices = new Vector3[mvl];
        for (int i = 0; i < mvl; i++)
        {
            vertices[i] = new Vertex(i, mesh.vertices[i], mesh.vertices[i], Vector3.zero);
            currMeshVertices[i] = mesh.vertices[i];
        }
    }

    void UpdateVertices()
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].UpdateVelocity(bounceSpeed);
            vertices[i].Settle(stiffness);

            vertices[i].currentVertexPosition += vertices[i].currentVelocity * Time.deltaTime;
            currMeshVertices[i] = vertices[i].currentVertexPosition;
        }

        mesh.vertices = currMeshVertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        meshCollider.sharedMesh = mesh;
    }

    public void ApplyPressureToPoint(Vector3 point, float pressure)
    {
        for (int i = 0; i < vertices.Length; i++)
        {
            vertices[i].ApplyPressureToVertex(transform, point, pressure);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        ContactPoint[] contactPoints = collision.contacts;
        for (int i = 0; i < contactPoints.Length; i++)
        {
            Vector3 inputPoint = contactPoints[i].point + (contactPoints[i].point * .1f);
            ApplyPressureToPoint(inputPoint, fallForce);
        }
    }
}
