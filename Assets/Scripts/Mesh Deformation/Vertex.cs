﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vertex
{
    public int verticeIndex;
    public Vector3 initialVertexPosition;
    public Vector3 currentVertexPosition;

    public Vector3 currentVelocity;

    public Vertex(int index, Vector3 initial, Vector3 current, Vector3 velocity)
    {
        verticeIndex = index;
        initialVertexPosition = initial;
        currentVertexPosition = current;
        currentVelocity = velocity;
    }

    public Vector3 CalculateDisplacement()
    {
        return currentVertexPosition - initialVertexPosition;
    }

    public void UpdateVelocity(float bounceSpeed)
    {
        currentVelocity = currentVelocity - CalculateDisplacement() * bounceSpeed * Time.deltaTime;
    }

    public void Settle(float stiffness)
    {
        currentVelocity *= 1f - stiffness * Time.deltaTime;
    }

    public void ApplyPressureToVertex(Transform transform, Vector3 position, float pressure)
    {
        Vector3 distanceVerticePoint = currentVertexPosition - transform.InverseTransformPoint(position);
        float adaptedPressure = pressure / (1f + distanceVerticePoint.sqrMagnitude);
        float velocity = adaptedPressure * Time.deltaTime;
        currentVelocity += distanceVerticePoint.normalized * velocity;
    }
}