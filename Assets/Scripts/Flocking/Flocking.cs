﻿using Boo.Lang.Environments;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Flocking : MonoBehaviour
{
    [Tooltip("Alignment factor with which the alignment vector is multiplied")]
    public float alignmentWeight = 1;
    [Tooltip("Cohesion factor with which the alignment vector is multiplied")]
    public float cohesionWeight = 1;
    [Tooltip("Separation factor with which the alignment vector is multiplied")]
    public float separationWeight = 3;
    [Tooltip("Radius at which the agents must maintain alignment")]
    public float alignmentRadius = 10;
    [Tooltip("Radius at which the agents must maintain cohesion")]
    public float cohesionRadius = 10;
    [Tooltip("Radius at which the agents must maintain separation")]
    public float separationRadius = 10;

    public Vector3 CalculateAlignment(Agent myAgent)
    {
        Vector3 alignment = Vector3.zero;

        List<Agent> neighbours = new List<Agent>();
        GetNeighbours(neighbours, myAgent, alignmentRadius);

        int neighborCount = 0;

        foreach (var agent in neighbours)
        {
            if (agent != myAgent)
            {
                if (agent.DistanceFrom(myAgent) < alignmentRadius)
                {
                    //If an agent is found within the radius, we add its velocity to the alignment vector
                    alignment.x += myAgent.rb.velocity.x;
                    alignment.y += myAgent.rb.velocity.y;
                    neighborCount++;
                }

            }
        }

        if (neighborCount == 0)
            return alignment;

        //We divide by the neighbour count to get the average and then normalize it to a get a vector of length 1
        alignment.x /= neighborCount;
        alignment.y /= neighborCount;
        alignment.Normalize();
        return alignment;
    }
    public Vector3 CalculateCohesion(Agent myAgent)
    {
        Vector3 cohesion = Vector3.zero;

        List<Agent> neighbours = new List<Agent>();
        GetNeighbours(neighbours, myAgent, alignmentRadius);

        int neighborCount = 0;

        foreach (var agent in neighbours)
        {
            if (agent != myAgent)
            {
                if (agent.DistanceFrom(myAgent) < cohesionRadius)
                {
                    //If an agent is found within the radius, we add its position to the cohesion vector
                    cohesion.x += agent.transform.position.x;
                    cohesion.y += agent.transform.position.y;
                    neighborCount++;
                }
            }
        }

        if (neighborCount == 0)
            return cohesion;

        //We divide by the neighbour count to get the average and then normalize it to a get a vector of length 1
        cohesion.x /= neighborCount;
        cohesion.y /= neighborCount;
        //We do not want the center of mass itself however, so we get the vector from agent to the center of mass
        cohesion = new Vector3(cohesion.x - myAgent.transform.position.x, cohesion.y - myAgent.transform.position.y);
        cohesion.Normalize();
        return cohesion;
    }
    public Vector3 CalculateSeparation(Agent myAgent)
    {
        Vector3 separation = Vector3.zero;

        List<Agent> neighbours = new List<Agent>();
        GetNeighbours(neighbours, myAgent, alignmentRadius);

        int neighborCount = 0;

        foreach (var agent in neighbours)
        {
            if (agent != myAgent)
            {
                if (agent.DistanceFrom(myAgent) < separationRadius)
                {
                    //If an agent is found within the radius, we add the direction from the neighbour to the current agent
                    //We subtract this from the separation radius however as we do not want every agent to have a similar effect on the current agent
                    //A farther agent needs to have a smaller effect on the current agent, thus we subtract from the radius
                    separation.x += separationRadius - (agent.transform.position.x - myAgent.transform.position.x);
                    separation.y += separationRadius - (agent.transform.position.y - myAgent.transform.position.y);
                    neighborCount++;
                }

            }
        }

        if (neighborCount == 0)
            return separation;

        //We divide by the neighbour count to get the average and then normalize it to a get a vector of length 1
        separation.x /= neighborCount;
        separation.y /= neighborCount;
        separation.Normalize();
        return separation;
    }

    public void Flock(Agent agent)
    {
        var alignment = CalculateAlignment(agent);
        var cohesion = CalculateCohesion(agent);
        var separation = CalculateSeparation(agent);

        agent.rb.velocity += new Vector3(alignment.x * alignmentWeight + cohesion.x * cohesionWeight + separation.x * separationWeight, alignment.y * alignmentWeight + cohesion.y * cohesionWeight + separation.y * separationWeight) * Time.deltaTime;

        agent.rb.velocity.Normalize();
    }

    void GetNeighbours(List<Agent> neighbours, Agent agent, float radius)
    {
        Collider[] neighbourcolliders = Physics.OverlapSphere(agent.transform.position, radius);
        foreach (var item in neighbourcolliders)
        {
            if (item.GetComponent<Agent>() && !neighbours.Contains(item.GetComponent<Agent>()))
                neighbours.Add(item.GetComponent<Agent>());
        }
    }
}
