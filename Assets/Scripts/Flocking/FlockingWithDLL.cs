﻿using System.Collections.Generic;
using UnityEngine;
using FlockingDLL;

public class FlockingWithDLL : MonoBehaviour
{
    [Tooltip("Alignment factor with which the alignment vector is multiplied")]
    public float alignmentWeight = 1;
    [Tooltip("Cohesion factor with which the alignment vector is multiplied")]
    public float cohesionWeight = 1;
    [Tooltip("Separation factor with which the alignment vector is multiplied")]
    public float separationWeight = 3;
    [Tooltip("Radius at which the agents must maintain alignment")]
    public float alignmentRadius = 10;
    [Tooltip("Radius at which the agents must maintain cohesion")]
    public float cohesionRadius = 10;
    [Tooltip("Radius at which the agents must maintain separation")]
    public float separationRadius = 10;

    public void Flock(AgentWithDLL agent)
    {
        var alignment = DLLVec3ToVector3(FlockingDLL.FlockerClass.CalculateAlignment(agent.myDLLAgent, GetNeighbours(agent, alignmentRadius), alignmentRadius));
        var cohesion = DLLVec3ToVector3(FlockingDLL.FlockerClass.CalculateCohesion(agent.myDLLAgent, GetNeighbours(agent, cohesionRadius), cohesionRadius));
        var separation = DLLVec3ToVector3(FlockingDLL.FlockerClass.CalculateSeparation(agent.myDLLAgent, GetNeighbours(agent, separationRadius), separationRadius));

        alignment.Normalize();
        cohesion.Normalize();
        separation.Normalize();

        agent.rb.velocity += new Vector3(alignment.x * alignmentWeight + cohesion.x * cohesionWeight + separation.x * separationWeight, alignment.y * alignmentWeight + cohesion.y * cohesionWeight + separation.y * separationWeight) * Time.deltaTime;

        agent.rb.velocity.Normalize();
    }

    List<FlockingDLL.Agent> GetNeighbours(AgentWithDLL agent, float radius)
    {
        List<FlockingDLL.Agent> neighbours = new List<FlockingDLL.Agent>();

        List<AgentWithDLL> _neighbours = new List<AgentWithDLL>();
        Collider[] neighbourcolliders = Physics.OverlapSphere(agent.transform.position, radius);
        foreach (var item in neighbourcolliders)
        {
            if (item.GetComponent<AgentWithDLL>() && !_neighbours.Contains(item.GetComponent<AgentWithDLL>()))
                _neighbours.Add(item.GetComponent<AgentWithDLL>());
        }

        foreach (var item in _neighbours)
        {
            neighbours.Add(item.myDLLAgent);
        }

        return neighbours;
    }

    public Vector3 DLLVec3ToVector3(DLLVec3 toConvert)
    {
        return new Vector3(toConvert.x, toConvert.y, toConvert.z);
    }

    public DLLVec3 Vector3ToDLLVec3(Vector3 toConvert)
    {
        return new DLLVec3(toConvert.x, toConvert.y, toConvert.z);
    }
}