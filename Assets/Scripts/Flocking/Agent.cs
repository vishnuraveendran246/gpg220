﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Agent : MonoBehaviour
{
    Flocking flockManager;

    [Tooltip("Goal that the agents are moving towards")]
    public GameObject goal;
    [Tooltip("Speed at which the agent moves")]
    public float speed;
    [Tooltip("Slowing radius for arrive")]
    public float slowingRadius;

    [HideInInspector] public Rigidbody rb;

    private void Start()
    {
        flockManager = FindObjectOfType<Flocking>();
        rb = GetComponent<Rigidbody>();
    }

    public float DistanceFrom(Agent agent)
    {
        return Vector3.Distance(this.transform.position, agent.transform.position);
    }

    private void Update()
    {
        Arrive();

        flockManager.Flock(this);
    }

    void Arrive()
    {
        Vector3 distToDest = goal.transform.position - transform.position;
        Vector3 desiredVel = distToDest.normalized * speed;
        desiredVel.z = 0;

        float slowDownFactor = Mathf.Clamp01(distToDest.magnitude / slowingRadius);

        Vector3 steering = desiredVel - rb.velocity;

        rb.velocity += steering * Time.deltaTime;

        rb.velocity *= slowDownFactor;

        transform.position += rb.velocity * Time.deltaTime;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawLine(transform.position, goal.transform.position);
    }
}